# README #

* Developed for a coding challenge

### What is this repository for? ###

* Crunchy Roll Coding Challenge
* Display images from a rest webservice
* Show images in various scaling and allow zoom

### Screenshots ###

![crunch.png](https://bitbucket.org/repo/9GMGe9/images/734128176-crunch.png)
### How do I get set up? ###

* Gradle sync

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* sarma.m.ravi@gmail.com
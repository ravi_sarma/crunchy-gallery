package com.rsarn.crunchyroll;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * Created by rsarn on 2/7/2016.
 */
public class CustomApplication extends Application {
    public static CustomApplication appInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;
        ActiveAndroid.initialize(this);
    }

    public static CustomApplication getInstance() {
        return appInstance;
    }
}

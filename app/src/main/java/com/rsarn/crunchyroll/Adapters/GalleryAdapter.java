package com.rsarn.crunchyroll.Adapters;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.rsarn.crunchyroll.Network.Model.ImageResponse;
import com.rsarn.crunchyroll.R;

import java.util.List;

/**
 * Created by rsarn on 2/7/2016.
 */
public class GalleryAdapter extends RecyclerView.Adapter {

    List<ImageResponse> mAdapterDataList;
    Context mContext;

    public GalleryAdapter(List adapterDataList, Context context) {
        this.mAdapterDataList = adapterDataList;
        this.mContext = context;
    }

    public static final class ImageViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imageView;
        AppCompatTextView textView;

        public ImageViewHolder(View itemView) {
            super(itemView);
            imageView = (AppCompatImageView) itemView.findViewById(R.id.image);
            textView = (AppCompatTextView) itemView.findViewById(R.id.caption);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_thumb, parent, false);
        return new ImageViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ImageResponse imageData = mAdapterDataList.get(position);
        ImageViewHolder holder1 = (ImageViewHolder) holder;
        if (imageData.getThumb() != null) {
            Glide.with(mContext).load(imageData.getThumb()).placeholder(R.drawable.ic_gallery_placeholder).into(holder1.imageView);
        } else {
            Glide.with(mContext).load(imageData.getOriginal()).thumbnail(0.1f).placeholder(R.drawable.ic_gallery_placeholder).into(holder1.imageView);
        }
        holder1.textView.setText(imageData.getCaption());
    }

    @Override
    public int getItemCount() {
        return mAdapterDataList.size();
    }

    public void setAdapterData(List list) {
        mAdapterDataList.clear();
        mAdapterDataList.addAll(list);
    }
}

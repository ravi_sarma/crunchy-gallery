package com.rsarn.crunchyroll.Network.Rest;

import com.rsarn.crunchyroll.Network.Model.ImageResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by rsarn on 2/7/2016.
 */
public class RestBuilder {
    private static String ENDPOINT = "http://www.crunchyroll.com/mobile-tech-challenge/";

    public static CrunchyRollAPI build() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(CrunchyRollAPI.class);
    }

    public static void fetchImages(final ImageChallengeStatusInterface callback) {
        CrunchyRollAPI imageAPI = build();
        Call<List<ImageResponse>> response = imageAPI.getMobileChallengeImages();
        response.enqueue(new Callback<List<ImageResponse>>() {
            @Override
            public void onResponse(Response<List<ImageResponse>> response) {
                if (response.isSuccess()) {
                    callback.onRequestComplete(response.body());
                } else {
                    callback.onRequestFail();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onRequestFail();
            }
        });
    }
}

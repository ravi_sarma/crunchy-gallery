package com.rsarn.crunchyroll.Network.Model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;

/**
 * Created by rsarn on 2/7/2016.
 */
public class ImageResponse extends Model {

    public ImageResponse() {
    }

    @Column(name = "uid",unique = true, index = true)
    public long ID;

    @Column(name = "caption")
    public String caption;

    @Column(name = "imageThumbnailURL")
    public String thumb;

    @Column(name = "imageOriginalURL")
    public String original;

    public ImageResponse(long ID, String caption, String thumb, String original) {
        this.ID = ID;
        this.caption = caption;
        this.thumb = thumb;
        this.original = original;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
}

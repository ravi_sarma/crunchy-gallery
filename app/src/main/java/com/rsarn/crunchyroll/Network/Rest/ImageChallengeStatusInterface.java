package com.rsarn.crunchyroll.Network.Rest;

import com.rsarn.crunchyroll.Network.Model.ImageResponse;

import java.util.List;

/**
 * Created by rsarn on 1/24/16.
 */
public interface ImageChallengeStatusInterface {

    public void onRequestComplete(List<ImageResponse> list);
    public void onRequestFail();
}

package com.rsarn.crunchyroll.Network.Rest;

import com.rsarn.crunchyroll.Network.Model.ImageResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by rsarn on 2/7/2016.
 */
public interface CrunchyRollAPI {

    @GET("images.json")
    Call<List<ImageResponse>> getMobileChallengeImages();
}

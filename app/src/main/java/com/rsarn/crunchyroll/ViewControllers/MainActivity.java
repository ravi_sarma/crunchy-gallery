package com.rsarn.crunchyroll.ViewControllers;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.rsarn.crunchyroll.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rsarn on 2/7/2016.
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GalleryFragment.OnFragmentInteractionListener {

    @Bind(R.id.drawer_layout)
    public DrawerLayout mDrawer;

    @Bind(R.id.nav_view)
    public NavigationView mNavigationView;

    @Bind(R.id.fragmentContainer)
    public RelativeLayout mFragmentContainer;
    GalleryFragment galleryFragment;
    Fragment mCurrentFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        galleryFragment = new GalleryFragment();
        //Set up fragment
        drawFragment(galleryFragment);
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void drawFragment(Fragment fragmentToDraw) {
        if (fragmentToDraw != mCurrentFragment && !fragmentToDraw.isAdded()) {
            mCurrentFragment = fragmentToDraw;
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(android.R.anim.fade_in, R.anim.slide_out_right, R.anim.slide_in_left, R.anim.slide_out_left);
            transaction.replace(R.id.fragmentContainer, fragmentToDraw);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        menu.clear();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_places) {
            if (!(mCurrentFragment instanceof GalleryFragment)) {
                drawFragment(new GalleryFragment());
            }
        } else if (id == R.id.nav_manage) {
            showSnackBarMessage("Snap, still under construction!", ContextCompat.getColor(this, R.color.colorAccent), Color.WHITE);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
                return;
            }
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (!(mCurrentFragment instanceof GalleryFragment)) {
            //This method is called when the up button is pressed. Just the pop back stack.
            FragmentManager fm = getSupportFragmentManager();
            if (fm.getBackStackEntryCount() > 1) {
                fm.popBackStackImmediate();
            }
        }
        return true;
    }

    private void showSnackBarMessage(String message, @ColorInt int backgroundColor, @ColorInt int textColor) {
        Snackbar snackbar = Snackbar.make(mFragmentContainer, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(backgroundColor);

        TextView snackBarText = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        snackBarText.setTextColor(textColor);
        snackbar.show();
    }

    @Override
    public void onSearchResultClick(long key) {

    }
}

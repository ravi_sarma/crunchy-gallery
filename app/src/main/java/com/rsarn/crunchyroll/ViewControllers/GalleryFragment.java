package com.rsarn.crunchyroll.ViewControllers;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.rsarn.crunchyroll.Adapters.GalleryAdapter;
import com.rsarn.crunchyroll.Network.Model.ImageResponse;
import com.rsarn.crunchyroll.Network.Rest.ImageChallengeStatusInterface;
import com.rsarn.crunchyroll.Network.Rest.RestBuilder;
import com.rsarn.crunchyroll.R;

import java.util.List;
import java.util.concurrent.Executors;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rsarn on 2/7/2016.
 */
public class GalleryFragment extends Fragment implements ImageChallengeStatusInterface {
    @Bind(R.id.innertoolbar)
    public Toolbar mToolbar;

    @Bind(R.id.recyclerView)
    public RecyclerView mRecyclerView;

    @Bind(R.id.noResultTextView)
    TextView noResultsTextView;

    GalleryAdapter recyclerViewAdapter;
    private OnFragmentInteractionListener mListener;

    public GalleryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
        RestBuilder.fetchImages(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflatedView = inflater.inflate(R.layout.fragment_location_result, container, false);
        ButterKnife.bind(this, inflatedView);

        // Set up Toolbar
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(mToolbar);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(true);
        appCompatActivity.getSupportActionBar().setTitle(getContext().getResources().getString(R.string.app_name));

        // Cook the burger icon
        DrawerLayout drawer = ((MainActivity) getActivity()).mDrawer;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        setupRecycler();
        return inflatedView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshAdapter();
    }

    private void setupRecycler() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (1);
            }
        });
        recyclerViewAdapter = new GalleryAdapter(queryLocalCache(), getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.notifyDataSetChanged();
    }

    private void refreshAdapter() {
        if (recyclerViewAdapter != null) {
            recyclerViewAdapter.setAdapterData(queryLocalCache());
            recyclerViewAdapter.notifyDataSetChanged();
            mRecyclerView.invalidate();
        }
    }

    private List queryLocalCache() {
        List<ImageResponse> imageResponseList = null;
        imageResponseList = new Select().from(ImageResponse.class).execute();
        if (mRecyclerView != null) {
            if (imageResponseList == null || imageResponseList.size() == 0) {
                mRecyclerView.setVisibility(View.GONE);
                noResultsTextView.setVisibility(View.VISIBLE);
            } else {
                mRecyclerView.setVisibility(View.VISIBLE);
                noResultsTextView.setVisibility(View.GONE);
            }
        }
        return imageResponseList;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRequestComplete(List<ImageResponse> list) {
        // update the db
        Executors.newSingleThreadExecutor().execute(new UpdateDatabase(list));
    }

    @Override
    public void onRequestFail() {

    }

    class UpdateDatabase implements Runnable {
        List<ImageResponse> mList;
        UpdateDatabase(List list) {
            this.mList = list;
        }
        @Override
        public void run() {
            if(mList!=null) {
                ActiveAndroid.beginTransaction();

                for(ImageResponse imageResponse : mList) {
                    long id = imageResponse.getCaption()!=null ? imageResponse.getCaption().hashCode() : imageResponse.getThumb().hashCode();
                    ImageResponse databaseImage = new Select().from(ImageResponse.class).where("uid =?", id).executeSingle();
                    if(databaseImage==null) {
                        imageResponse.setID(id);
                        imageResponse.save();
                    } else {
                        databaseImage.setCaption(imageResponse.getCaption());
                        databaseImage.setOriginal(imageResponse.getOriginal());
                        databaseImage.setThumb(imageResponse.getThumb());
                        databaseImage.save();
                    }
                }
                ActiveAndroid.setTransactionSuccessful();
                ActiveAndroid.endTransaction();
            }
            GalleryFragment.this.getActivity().runOnUiThread(refreshAdapter);
        }
    }

    Runnable refreshAdapter = new Runnable() {
        @Override
        public void run() {
            refreshAdapter();
        }
    };

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSearchResultClick(long key);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

}
